package com.assignment2.javadataaccess.controllers;

import com.assignment2.javadataaccess.dap.SearchRepository;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class SearchController {

    SearchRepository srep = new SearchRepository();

    //Getting the user search and display song
    @RequestMapping(value = "song/search={value}", method = RequestMethod.GET)
    public String search(@PathVariable String value, Model model) {
        model.addAttribute("values", srep.getSongInfo(value));
        model.addAttribute("searched", value);
        return "search";
    }
}
