FROM openjdk:16
ADD target/thymeleafsqlite-1.jar tl-sqlite.jar
ENTRYPOINT [ "java", "-jar", "tl-sqlite.jar" ]